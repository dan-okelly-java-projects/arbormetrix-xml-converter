package com.arbormetrix.xml.converter;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication(scanBasePackages = "com.arbormetrix.xml.converter")
public class XmlConverterApplication {

    public static void main(String[] args) {
        run(XmlConverterApplication.class, args);
    }
}
